<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A93306">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Six new queries</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A93306 of text R211412 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.22[41]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A93306</idno>
    <idno type="STC">Wing S3916</idno>
    <idno type="STC">Thomason 669.f.22[41]</idno>
    <idno type="STC">ESTC R211412</idno>
    <idno type="EEBO-CITATION">99870140</idno>
    <idno type="PROQUEST">99870140</idno>
    <idno type="VID">163624</idno>
    <idno type="PROQUESTGOID">2240939484</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A93306)</note>
    <note>Transcribed from: (Early English Books Online ; image set 163624)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 247:669f22[41])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Six new queries</title>
      <author>Thomason, George, d. 1666, attributed name.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1659]</date>
     </publicationStmt>
     <notesStmt>
      <note>Imprint from Wing.</note>
      <note>Relating to the Army and Parliament and questioning whether the Parliament will be free from control by the Army.</note>
      <note>Possible authorship of Thomason from Thomason catalogue.</note>
      <note>Annotation on Thomason copy: "xber [i.e., December] 29. 1659"; "NB. G.T. [i.e. George Thomason?]".</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>England and Wales. -- Army -- Early works to 1800.</term>
     <term>England and Wales. -- Parliament -- Early works to 1800.</term>
     <term>Great Britain -- Politics and government -- 1649-1660 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A93306</ep:tcp>
    <ep:estc> R211412</ep:estc>
    <ep:stc> (Thomason 669.f.22[41]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>Six new queries.</ep:title>
    <ep:author>[Thomason, George] </ep:author>
    <ep:publicationYear>1659</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>294</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-07</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2007-07</date>
    <label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-09</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2007-09</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A93306-e10">
  <body xml:id="A93306-e20">
   <pb facs="tcp:163624:1" rend="simple:additions" xml:id="A93306-001-a"/>
   <div type="queries" xml:id="A93306-e30">
    <head xml:id="A93306-e40">
     <w lemma="six" pos="crd" xml:id="A93306-001-a-0010">Six</w>
     <w lemma="new" pos="j" xml:id="A93306-001-a-0020">New</w>
     <w lemma="query" pos="n2" xml:id="A93306-001-a-0030">Queries</w>
     <pc unit="sentence" xml:id="A93306-001-a-0040">.</pc>
    </head>
    <p xml:id="A93306-e50">
     <label xml:id="A93306-e60">
      <w lemma="i." pos="ab" xml:id="A93306-001-a-0050">I.</w>
      <pc unit="sentence" xml:id="A93306-001-a-0060"/>
     </label>
     <w lemma="whither" pos="crq" xml:id="A93306-001-a-0070">WHether</w>
     <w lemma="or" pos="cc" xml:id="A93306-001-a-0080">or</w>
     <w lemma="no" pos="avx-d" xml:id="A93306-001-a-0090">no</w>
     <pc xml:id="A93306-001-a-0100">,</pc>
     <w lemma="any" pos="d" xml:id="A93306-001-a-0110">any</w>
     <w lemma="rational" pos="j" xml:id="A93306-001-a-0120">rational</w>
     <w lemma="man" pos="n1" xml:id="A93306-001-a-0130">man</w>
     <w lemma="of" pos="acp" xml:id="A93306-001-a-0140">of</w>
     <hi xml:id="A93306-e70">
      <w lemma="England" pos="nn1" xml:id="A93306-001-a-0150">England</w>
      <pc xml:id="A93306-001-a-0160">,</pc>
     </hi>
     <w lemma="can" pos="vmb" xml:id="A93306-001-a-0170">can</w>
     <w lemma="or" pos="cc" xml:id="A93306-001-a-0180">or</w>
     <w lemma="may" pos="vmb" xml:id="A93306-001-a-0190">may</w>
     <w lemma="expect" pos="vvi" xml:id="A93306-001-a-0200">expect</w>
     <w lemma="any" pos="d" xml:id="A93306-001-a-0210">any</w>
     <w lemma="good" pos="j" xml:id="A93306-001-a-0220">good</w>
     <w lemma="from" pos="acp" xml:id="A93306-001-a-0230">from</w>
     <w lemma="a" pos="d" xml:id="A93306-001-a-0240">a</w>
     <hi xml:id="A93306-e80">
      <w lemma="parliament" pos="n1" xml:id="A93306-001-a-0250">Parliament</w>
      <pc xml:id="A93306-001-a-0260">,</pc>
     </hi>
     <w lemma="when" pos="crq" xml:id="A93306-001-a-0270">when</w>
     <w lemma="a" pos="d" xml:id="A93306-001-a-0280">an</w>
     <w lemma="army" pos="n1" rend="hi" xml:id="A93306-001-a-0290">Army</w>
     <w lemma="be" pos="vvz" xml:id="A93306-001-a-0300">is</w>
     <w lemma="in" pos="acp" xml:id="A93306-001-a-0310">in</w>
     <w lemma="power" pos="n1" xml:id="A93306-001-a-0320">power</w>
     <w lemma="at" pos="acp" xml:id="A93306-001-a-0330">at</w>
     <w lemma="the" pos="d" xml:id="A93306-001-a-0340">the</w>
     <w lemma="same" pos="d" xml:id="A93306-001-a-0350">same</w>
     <w lemma="time" pos="n1" xml:id="A93306-001-a-0360">time</w>
     <w lemma="in" pos="acp" xml:id="A93306-001-a-0370">in</w>
     <w lemma="the" pos="d" xml:id="A93306-001-a-0380">the</w>
     <w lemma="nation" pos="n1" xml:id="A93306-001-a-0390">Nation</w>
     <pc unit="sentence" xml:id="A93306-001-a-0400">?</pc>
    </p>
    <p xml:id="A93306-e100">
     <label xml:id="A93306-e110">
      <w lemma="2" pos="crd" xml:id="A93306-001-a-0410">II.</w>
      <pc unit="sentence" xml:id="A93306-001-a-0420"/>
     </label>
     <w lemma="whether" pos="cs" xml:id="A93306-001-a-0430">Whether</w>
     <w lemma="or" pos="cc" xml:id="A93306-001-a-0440">or</w>
     <w lemma="no" pos="avx-d" xml:id="A93306-001-a-0450">no</w>
     <pc xml:id="A93306-001-a-0460">,</pc>
     <w lemma="a" pos="d" xml:id="A93306-001-a-0470">a</w>
     <w lemma="parliament" pos="n1" rend="hi" xml:id="A93306-001-a-0480">Parliament</w>
     <w lemma="constitute" pos="vvn" xml:id="A93306-001-a-0490">Constituted</w>
     <w lemma="as" pos="acp" xml:id="A93306-001-a-0500">as</w>
     <w lemma="the" pos="d" xml:id="A93306-001-a-0510">the</w>
     <w lemma="last" pos="ord" xml:id="A93306-001-a-0520">last</w>
     <w lemma="be" pos="vvd" xml:id="A93306-001-a-0530">was</w>
     <pc join="right" xml:id="A93306-001-a-0540">(</pc>
     <w lemma="before" pos="acp" xml:id="A93306-001-a-0550">before</w>
     <w lemma="the" pos="d" xml:id="A93306-001-a-0560">the</w>
     <w lemma="soldier" pos="n2" xml:id="A93306-001-a-0570">Soldiers</w>
     <w lemma="dissolve" pos="vvn" xml:id="A93306-001-a-0580">dissolved</w>
     <w lemma="it" pos="pn" xml:id="A93306-001-a-0590">it</w>
     <pc xml:id="A93306-001-a-0600">)</pc>
     <w lemma="deserve" pos="vvz" xml:id="A93306-001-a-0610">deserves</w>
     <w lemma="any" pos="d" xml:id="A93306-001-a-0620">any</w>
     <w lemma="other" pos="d" xml:id="A93306-001-a-0630">other</w>
     <w lemma="name" pos="n1" xml:id="A93306-001-a-0640">Name</w>
     <pc xml:id="A93306-001-a-0650">,</pc>
     <w lemma="title" pos="n1" xml:id="A93306-001-a-0660">Title</w>
     <pc xml:id="A93306-001-a-0670">,</pc>
     <w lemma="or" pos="cc" xml:id="A93306-001-a-0680">or</w>
     <w lemma="appellation" pos="n1" xml:id="A93306-001-a-0690">Appellation</w>
     <pc xml:id="A93306-001-a-0700">,</pc>
     <w lemma="than" pos="cs" xml:id="A93306-001-a-0710">than</w>
     <w lemma="the" pos="d" xml:id="A93306-001-a-0720">the</w>
     <hi xml:id="A93306-e130">
      <w lemma="rump" pos="n1" xml:id="A93306-001-a-0730">RUMP</w>
      <pc xml:id="A93306-001-a-0740">;</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A93306-001-a-0750">And</w>
     <w lemma="whether" pos="cs" xml:id="A93306-001-a-0760">whether</w>
     <w lemma="any" pos="d" xml:id="A93306-001-a-0770">any</w>
     <w lemma="one" pos="pi" xml:id="A93306-001-a-0780">one</w>
     <w lemma="will" pos="vmb" xml:id="A93306-001-a-0790">will</w>
     <pc xml:id="A93306-001-a-0800">,</pc>
     <w lemma="or" pos="cc" xml:id="A93306-001-a-0810">or</w>
     <w lemma="dare" pos="vvb" xml:id="A93306-001-a-0820">dare</w>
     <w lemma="presume" pos="vvi" xml:id="A93306-001-a-0830">presume</w>
     <w lemma="to" pos="prt" xml:id="A93306-001-a-0840">to</w>
     <w lemma="call" pos="vvi" xml:id="A93306-001-a-0850">call</w>
     <w lemma="that" pos="d" xml:id="A93306-001-a-0860">that</w>
     <w lemma="parliament" pos="n1" rend="hi" xml:id="A93306-001-a-0870">Parliament</w>
     <pc join="right" xml:id="A93306-001-a-0880">(</pc>
     <w lemma="entire" pos="av-j" xml:id="A93306-001-a-0890">entirely</w>
     <w lemma="assemble" pos="vvn" xml:id="A93306-001-a-0900">assembled</w>
     <pc xml:id="A93306-001-a-0910">)</pc>
     <w lemma="by" pos="acp" xml:id="A93306-001-a-0920">by</w>
     <w lemma="any" pos="d" xml:id="A93306-001-a-0930">any</w>
     <w lemma="other" pos="d" xml:id="A93306-001-a-0940">other</w>
     <w lemma="name" pos="n1" xml:id="A93306-001-a-0950">Name</w>
     <pc xml:id="A93306-001-a-0960">,</pc>
     <w lemma="title" pos="n1" xml:id="A93306-001-a-0970">Title</w>
     <pc xml:id="A93306-001-a-0980">,</pc>
     <w lemma="or" pos="cc" xml:id="A93306-001-a-0990">or</w>
     <w lemma="appellation" pos="n1" xml:id="A93306-001-a-1000">Appellation</w>
     <pc xml:id="A93306-001-a-1010">,</pc>
     <w lemma="but" pos="acp" xml:id="A93306-001-a-1020">but</w>
     <w lemma="a" pos="d" xml:id="A93306-001-a-1030">a</w>
     <hi xml:id="A93306-e150">
      <w lemma="free" pos="j" xml:id="A93306-001-a-1040">Free</w>
      <w lemma="parliament" pos="n1" xml:id="A93306-001-a-1050">Parliament</w>
      <w lemma="of" pos="acp" xml:id="A93306-001-a-1060">of</w>
      <w lemma="England" pos="nn1" xml:id="A93306-001-a-1070">England</w>
      <pc unit="sentence" xml:id="A93306-001-a-1080">?</pc>
     </hi>
    </p>
    <p xml:id="A93306-e160">
     <label xml:id="A93306-e170">
      <w lemma="3" pos="crd" xml:id="A93306-001-a-1090">III.</w>
      <pc unit="sentence" xml:id="A93306-001-a-1100"/>
     </label>
     <w lemma="whether" pos="cs" xml:id="A93306-001-a-1110">Whether</w>
     <w lemma="or" pos="cc" xml:id="A93306-001-a-1120">or</w>
     <w lemma="no" pos="dx" xml:id="A93306-001-a-1130">no</w>
     <w lemma="this" pos="d" xml:id="A93306-001-a-1140">this</w>
     <w lemma="nation" pos="n1" rend="hi" xml:id="A93306-001-a-1150">Nation</w>
     <w lemma="may" pos="vmb" xml:id="A93306-001-a-1160">may</w>
     <w lemma="expect" pos="vvi" xml:id="A93306-001-a-1170">expect</w>
     <w lemma="any" pos="d" xml:id="A93306-001-a-1180">any</w>
     <w lemma="good" pos="j" xml:id="A93306-001-a-1190">good</w>
     <pc xml:id="A93306-001-a-1200">,</pc>
     <w lemma="by" pos="acp" xml:id="A93306-001-a-1210">by</w>
     <pc xml:id="A93306-001-a-1220">,</pc>
     <w lemma="or" pos="cc" xml:id="A93306-001-a-1230">or</w>
     <w lemma="from" pos="acp" xml:id="A93306-001-a-1240">from</w>
     <w lemma="a" pos="d" xml:id="A93306-001-a-1250">a</w>
     <hi xml:id="A93306-e190">
      <w lemma="parliament" pos="n1" xml:id="A93306-001-a-1260">Parliament</w>
      <pc xml:id="A93306-001-a-1270">,</pc>
     </hi>
     <w lemma="that" pos="cs" xml:id="A93306-001-a-1280">that</w>
     <w lemma="have" pos="vvz" xml:id="A93306-001-a-1290">hath</w>
     <w lemma="any" pos="d" xml:id="A93306-001-a-1300">any</w>
     <w lemma="soldier" pos="n2" xml:id="A93306-001-a-1310">Soldiers</w>
     <w lemma="to" pos="prt" xml:id="A93306-001-a-1320">to</w>
     <w lemma="sit" pos="vvi" xml:id="A93306-001-a-1330">sit</w>
     <pc xml:id="A93306-001-a-1340">,</pc>
     <w lemma="act" pos="n1" xml:id="A93306-001-a-1350">act</w>
     <pc xml:id="A93306-001-a-1360">,</pc>
     <w lemma="or" pos="cc" xml:id="A93306-001-a-1370">or</w>
     <w lemma="vote" pos="n1" xml:id="A93306-001-a-1380">vote</w>
     <w lemma="in" pos="acp" xml:id="A93306-001-a-1390">in</w>
     <w lemma="that" pos="d" xml:id="A93306-001-a-1400">that</w>
     <w lemma="house" pos="n1" xml:id="A93306-001-a-1410">House</w>
     <pc xml:id="A93306-001-a-1420">,</pc>
     <w lemma="or" pos="cc" xml:id="A93306-001-a-1430">or</w>
     <w lemma="for" pos="acp" xml:id="A93306-001-a-1440">for</w>
     <w lemma="it" pos="pn" xml:id="A93306-001-a-1450">it</w>
     <pc xml:id="A93306-001-a-1460">,</pc>
     <w lemma="that" pos="cs" xml:id="A93306-001-a-1470">that</w>
     <w lemma="have" pos="vvb" xml:id="A93306-001-a-1480">have</w>
     <w lemma="any" pos="d" xml:id="A93306-001-a-1490">any</w>
     <w lemma="command" pos="n1" xml:id="A93306-001-a-1500">Command</w>
     <w lemma="derive" pos="vvn" xml:id="A93306-001-a-1510">derived</w>
     <w lemma="from" pos="acp" xml:id="A93306-001-a-1520">from</w>
     <w lemma="such" pos="d" xml:id="A93306-001-a-1530">such</w>
     <w lemma="a" pos="d" xml:id="A93306-001-a-1540">a</w>
     <hi xml:id="A93306-e200">
      <w lemma="parliament" pos="n1" xml:id="A93306-001-a-1550">Parliament</w>
      <pc unit="sentence" xml:id="A93306-001-a-1560">?</pc>
     </hi>
    </p>
    <p xml:id="A93306-e210">
     <label xml:id="A93306-e220">
      <w lemma="4" pos="crd" xml:id="A93306-001-a-1570">IV.</w>
      <pc unit="sentence" xml:id="A93306-001-a-1580"/>
     </label>
     <w lemma="whether" pos="cs" xml:id="A93306-001-a-1590">Whether</w>
     <w lemma="or" pos="cc" xml:id="A93306-001-a-1600">or</w>
     <w lemma="no" pos="dx" xml:id="A93306-001-a-1610">no</w>
     <w lemma="the" pos="d" xml:id="A93306-001-a-1620">the</w>
     <w lemma="person" pos="n2" xml:id="A93306-001-a-1630">persons</w>
     <w lemma="now" pos="av" xml:id="A93306-001-a-1640">now</w>
     <w lemma="sit" pos="vvg" xml:id="A93306-001-a-1650">sitting</w>
     <pc xml:id="A93306-001-a-1660">,</pc>
     <w lemma="intend" pos="vvb" xml:id="A93306-001-a-1670">intend</w>
     <w lemma="any" pos="d" xml:id="A93306-001-a-1680">any</w>
     <w lemma="other" pos="d" xml:id="A93306-001-a-1690">other</w>
     <w lemma="thing" pos="n1" xml:id="A93306-001-a-1700">thing</w>
     <w lemma="than" pos="cs" xml:id="A93306-001-a-1710">than</w>
     <w lemma="to" pos="acp" xml:id="A93306-001-a-1720">to</w>
     <w lemma="lord" pos="n1" xml:id="A93306-001-a-1730">Lord</w>
     <w lemma="it" pos="pn" xml:id="A93306-001-a-1740">it</w>
     <w lemma="over" pos="acp" xml:id="A93306-001-a-1750">over</w>
     <w lemma="their" pos="po" xml:id="A93306-001-a-1760">their</w>
     <w lemma="brethren" pos="n2" xml:id="A93306-001-a-1770">Brethren</w>
     <w lemma="in" pos="acp" xml:id="A93306-001-a-1780">in</w>
     <w lemma="this" pos="d" xml:id="A93306-001-a-1790">this</w>
     <w lemma="nation" pos="n1" xml:id="A93306-001-a-1800">Nation</w>
     <pc xml:id="A93306-001-a-1810">,</pc>
     <w lemma="while" pos="cs" reg="whilst" xml:id="A93306-001-a-1820">whilest</w>
     <w lemma="they" pos="pns" xml:id="A93306-001-a-1830">they</w>
     <w lemma="take" pos="vvb" xml:id="A93306-001-a-1840">take</w>
     <w lemma="such" pos="d" xml:id="A93306-001-a-1850">such</w>
     <w lemma="course" pos="n2" xml:id="A93306-001-a-1860">courses</w>
     <w lemma="as" pos="acp" xml:id="A93306-001-a-1870">as</w>
     <w lemma="they" pos="pns" xml:id="A93306-001-a-1880">they</w>
     <w lemma="have" pos="vvb" xml:id="A93306-001-a-1890">have</w>
     <w lemma="do" pos="vvn" xml:id="A93306-001-a-1900">done</w>
     <pc xml:id="A93306-001-a-1910">,</pc>
     <w lemma="and" pos="cc" xml:id="A93306-001-a-1920">and</w>
     <w lemma="fill" pos="vvb" xml:id="A93306-001-a-1930">fill</w>
     <w lemma="not" pos="xx" xml:id="A93306-001-a-1940">not</w>
     <w lemma="up" pos="acp" xml:id="A93306-001-a-1950">up</w>
     <w lemma="the" pos="d" xml:id="A93306-001-a-1960">the</w>
     <w lemma="house" pos="n1" xml:id="A93306-001-a-1970">House</w>
     <w lemma="with" pos="acp" xml:id="A93306-001-a-1980">with</w>
     <w lemma="their" pos="po" xml:id="A93306-001-a-1990">their</w>
     <w lemma="fellow" pos="n1" xml:id="A93306-001-a-2000">fellow</w>
     <w lemma="member" pos="n2" xml:id="A93306-001-a-2010">Members</w>
     <w lemma="of" pos="acp" xml:id="A93306-001-a-2020">of</w>
     <w lemma="the" pos="d" xml:id="A93306-001-a-2030">the</w>
     <w lemma="same" pos="d" xml:id="A93306-001-a-2040">same</w>
     <w lemma="call" pos="vvb" xml:id="A93306-001-a-2050">Call</w>
     <w lemma="with" pos="acp" xml:id="A93306-001-a-2060">with</w>
     <w lemma="themselves" pos="pr" xml:id="A93306-001-a-2070">themselves</w>
     <pc unit="sentence" xml:id="A93306-001-a-2080">?</pc>
    </p>
    <p xml:id="A93306-e230">
     <label xml:id="A93306-e240">
      <w lemma="v." pos="sy" xml:id="A93306-001-a-2090">V.</w>
      <pc unit="sentence" xml:id="A93306-001-a-2100"/>
     </label>
     <w lemma="whether" pos="cs" xml:id="A93306-001-a-2110">Whether</w>
     <w lemma="or" pos="cc" xml:id="A93306-001-a-2120">or</w>
     <w lemma="no" pos="avx-d" xml:id="A93306-001-a-2130">no</w>
     <w lemma="it" pos="pn" xml:id="A93306-001-a-2140">it</w>
     <w lemma="be" pos="vvi" xml:id="A93306-001-a-2150">be</w>
     <w lemma="a" pos="d" xml:id="A93306-001-a-2160">a</w>
     <hi xml:id="A93306-e250">
      <w lemma="free" pos="j" xml:id="A93306-001-a-2170">Free</w>
      <w lemma="parliament" pos="n1" xml:id="A93306-001-a-2180">Parliament</w>
     </hi>
     <w lemma="of" pos="acp" xml:id="A93306-001-a-2190">of</w>
     <hi xml:id="A93306-e260">
      <w lemma="England" pos="nn1" xml:id="A93306-001-a-2200">England</w>
      <pc xml:id="A93306-001-a-2210">,</pc>
     </hi>
     <w lemma="if" pos="cs" xml:id="A93306-001-a-2220">if</w>
     <w lemma="there" pos="av" xml:id="A93306-001-a-2230">there</w>
     <w lemma="be" pos="vvi" xml:id="A93306-001-a-2240">be</w>
     <w lemma="any" pos="d" xml:id="A93306-001-a-2250">any</w>
     <w lemma="limitation" pos="n1" xml:id="A93306-001-a-2260">Limitation</w>
     <w lemma="or" pos="cc" xml:id="A93306-001-a-2270">or</w>
     <w lemma="restriction" pos="n1" xml:id="A93306-001-a-2280">Restriction</w>
     <w lemma="put" pos="vvn" xml:id="A93306-001-a-2290">put</w>
     <w lemma="upon" pos="acp" xml:id="A93306-001-a-2300">upon</w>
     <w lemma="they" pos="pno" xml:id="A93306-001-a-2310">them</w>
     <w lemma="that" pos="cs" xml:id="A93306-001-a-2320">that</w>
     <w lemma="be" pos="vvb" xml:id="A93306-001-a-2330">are</w>
     <w lemma="to" pos="prt" xml:id="A93306-001-a-2340">to</w>
     <w lemma="choose" pos="vvi" xml:id="A93306-001-a-2350">choose</w>
     <pc xml:id="A93306-001-a-2360">,</pc>
     <w lemma="such" pos="d" xml:id="A93306-001-a-2370">such</w>
     <w lemma="person" pos="n2" xml:id="A93306-001-a-2380">persons</w>
     <w lemma="to" pos="prt" xml:id="A93306-001-a-2390">to</w>
     <w lemma="sit" pos="vvi" xml:id="A93306-001-a-2400">sit</w>
     <w lemma="and" pos="cc" xml:id="A93306-001-a-2410">and</w>
     <w lemma="act" pos="vvi" xml:id="A93306-001-a-2420">act</w>
     <w lemma="for" pos="acp" xml:id="A93306-001-a-2430">for</w>
     <w lemma="they" pos="pno" xml:id="A93306-001-a-2440">them</w>
     <w lemma="in" pos="acp" xml:id="A93306-001-a-2450">in</w>
     <w lemma="that" pos="d" xml:id="A93306-001-a-2460">that</w>
     <w lemma="assembly" pos="n1" xml:id="A93306-001-a-2470">Assembly</w>
     <pc xml:id="A93306-001-a-2480">,</pc>
     <w lemma="as" pos="acp" xml:id="A93306-001-a-2490">as</w>
     <w lemma="they" pos="pns" xml:id="A93306-001-a-2500">they</w>
     <w lemma="be" pos="vvb" xml:id="A93306-001-a-2510">are</w>
     <w lemma="willing" pos="j" xml:id="A93306-001-a-2520">willing</w>
     <w lemma="to" pos="prt" xml:id="A93306-001-a-2530">to</w>
     <w lemma="repose" pos="vvi" xml:id="A93306-001-a-2540">repose</w>
     <w lemma="trust" pos="n1" xml:id="A93306-001-a-2550">trust</w>
     <w lemma="in" pos="acp" xml:id="A93306-001-a-2560">in</w>
     <pc unit="sentence" xml:id="A93306-001-a-2570">?</pc>
    </p>
    <p xml:id="A93306-e270">
     <label xml:id="A93306-e280">
      <w lemma="6" pos="crd" xml:id="A93306-001-a-2580">VI</w>
      <pc unit="sentence" xml:id="A93306-001-a-2590">.</pc>
     </label>
     <w lemma="whether" pos="cs" xml:id="A93306-001-a-2600">Whether</w>
     <w lemma="or" pos="cc" xml:id="A93306-001-a-2610">or</w>
     <w lemma="no" pos="avx-d" xml:id="A93306-001-a-2620">no</w>
     <w lemma="we" pos="pns" xml:id="A93306-001-a-2630">we</w>
     <w lemma="have" pos="vvb" xml:id="A93306-001-a-2640">have</w>
     <w lemma="have" pos="vvn" xml:id="A93306-001-a-2650">had</w>
     <w lemma="any" pos="d" xml:id="A93306-001-a-2660">any</w>
     <w lemma="peace" pos="n1" xml:id="A93306-001-a-2670">Peace</w>
     <w lemma="in" pos="acp" xml:id="A93306-001-a-2680">in</w>
     <w lemma="this" pos="d" xml:id="A93306-001-a-2690">this</w>
     <w lemma="nation" pos="n1" xml:id="A93306-001-a-2700">Nation</w>
     <pc xml:id="A93306-001-a-2710">,</pc>
     <w lemma="ever" pos="av" xml:id="A93306-001-a-2720">ever</w>
     <w lemma="since" pos="acp" xml:id="A93306-001-a-2730">since</w>
     <w lemma="the" pos="d" xml:id="A93306-001-a-2740">the</w>
     <w lemma="military" pos="j" rend="hi" xml:id="A93306-001-a-2750">Military</w>
     <w lemma="man" pos="n1" xml:id="A93306-001-a-2760">Man</w>
     <w lemma="have" pos="vvz" xml:id="A93306-001-a-2770">hath</w>
     <w lemma="have" pos="vvn" xml:id="A93306-001-a-2780">had</w>
     <w lemma="any" pos="d" xml:id="A93306-001-a-2790">any</w>
     <w lemma="power" pos="n1" xml:id="A93306-001-a-2800">power</w>
     <w lemma="in" pos="acp" xml:id="A93306-001-a-2810">in</w>
     <w lemma="it" pos="pn" xml:id="A93306-001-a-2820">it</w>
     <pc xml:id="A93306-001-a-2830">,</pc>
     <w lemma="or" pos="cc" xml:id="A93306-001-a-2840">or</w>
     <w lemma="ever" pos="av" xml:id="A93306-001-a-2850">ever</w>
     <w lemma="shall" pos="vmb" xml:id="A93306-001-a-2860">shall</w>
     <w lemma="expect" pos="vvi" xml:id="A93306-001-a-2870">expect</w>
     <w lemma="any" pos="d" xml:id="A93306-001-a-2880">any</w>
     <w lemma="while" pos="cs" reg="whilst" xml:id="A93306-001-a-2890">whilest</w>
     <w lemma="a" pos="d" xml:id="A93306-001-a-2900">a</w>
     <w lemma="parliament" pos="n1" rend="hi" xml:id="A93306-001-a-2910">Parliament</w>
     <w lemma="commissionates" pos="nng1" reg="Commissionates'" xml:id="A93306-001-a-2920">Commissionates</w>
     <w lemma="then" pos="av" xml:id="A93306-001-a-2930">them</w>
     <pc unit="sentence" xml:id="A93306-001-a-2940">?</pc>
    </p>
   </div>
  </body>
 </text>
</TEI>
